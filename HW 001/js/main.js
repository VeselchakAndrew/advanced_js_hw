function Hamburger(size, stuffing) {

    try {
        if (!size || size.join().indexOf("SIZE") < 0 && !stuffing || stuffing.join().indexOf("STUFFING") < 0) {
            throw "incorectData";
        }

        let toppings = [];
        this.toppings = toppings;
        this.size = size;
        this.stuffing = stuffing;
    }

    catch (error) {
        console.error(incorrectDataForHamburger.message);
    }
}

Hamburger.SIZE_SMALL = ["SIZE_SMALL", 50, 20];
Hamburger.SIZE_LARGE = ["SIZE_LARGE", 100, 40];
Hamburger.STUFFING_CHEESE = ["STUFFING_CHEESE", 10, 20];
Hamburger.STUFFING_SALAD = ["STUFFING_SALAD", 20, 5];
Hamburger.STUFFING_POTATO = ["STUFFING_POTATO", 15, 10];
Hamburger.TOPPING_MAYO = ["TOPPING_MAYO", 20, 5];
Hamburger.TOPPING_SPICE = ["TOPPING_SPICE", 15, 0];


Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping || topping.join().indexOf("TOPPING") < 0) {
            throw "incorrectData";
        }

        if (this.toppings.includes(topping)) {
            throw "duplicateData";
        }

        else {
            this.toppings.push(topping);
        }
    } catch (error) {

        if (error === "incorrectData") {
            console.error(incorrectDataForTopping.message)
        }
        else {
            console.error(duplicateDataForTopping.message)
        }
    }
}

Hamburger.prototype.removeAllTopping = function () {
    this.toppings = [];
}

Hamburger.prototype.removeTopping = function (topping) {
    let index = this.toppings.indexOf(topping);
    if (this.toppings.includes(topping)) {
        this.toppings.splice(index, index);
    }
}

Hamburger.prototype.getToppings = function () {
    let toppingsInfo = [];
    for (let i = 0; i < this.toppings.length; i++) {
        toppingsInfo.push(this.toppings[i][0]);
    }
    return toppingsInfo;
}

Hamburger.prototype.getSize = function () {
    return this.size[0];
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing[0];
}

Hamburger.prototype.calculatePrice = function () {
    let toppingsPrice = 0;

    if (this.toppings) {
        for (let i = 0; i < this.toppings.length; i++) {
            toppingsPrice += this.toppings[i][1];
        }
    }

    return this.size[1] + this.stuffing[1] + toppingsPrice;
}

Hamburger.prototype.calculateCalories = function () {
    let toppingsCalories = 0;

    if (this.toppings) {
        for (let i = 0; i < this.toppings.length; i++) {
            toppingsCalories += this.toppings[i][2];
        }
    }
    return this.size[2] + this.stuffing[2] + toppingsCalories;
}

// Hamburger exeptions
function HamburgerException(message) {
    this.message = message;
}

const incorrectDataForHamburger = new HamburgerException("Incorrect data for creating hamburger. Please check you data or create new Size or Stuffing.");
const incorrectDataForTopping = new HamburgerException("Incorrect data for adding topping. Please check you data or create new Topping.");
const duplicateDataForTopping = new HamburgerException("Duplicate topping detected.");









