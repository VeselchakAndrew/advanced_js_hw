class Hamburger {
	constructor(size, stuffing) {
		if (
			size.hasOwnProperty("type") &&
			size.type === "size" &&
			stuffing.hasOwnProperty("type") &&
			stuffing.type === "stuffing"
		) {
			this.toppings = [];
			this.size = size;
			this.stuffing = stuffing;
		} else {
			throw new HamburgerException(
				"Incorrect data for creating hamburger. Please check you data or create new Size or Stuffing."
			);
		}
	}

	static SIZE_SMALL = {
		name: "SIZE_SMALL",
		type: "size",
		price: 50,
		calories: 20,
	};

	static SIZE_LARGE = {
		name: "SIZE_LARGE",
		type: "size",
		price: 100,
		calories: 40,
	};

	static STUFFING_CHEESE = {
		name: "STUFFING_CHEESE",
		type: "stuffing",
		price: 10,
		calories: 20,
	};

	static STUFFING_SALAD = {
		name: "STUFFING_SALAD",
		type: "stuffing",
		price: 20,
		calories: 5,
	};

	static STUFFING_POTATO = {
		name: "STUFFING_POTATO",
		type: "stuffing",
		price: 15,
		calories: 10,
	};

	static TOPPING_MAYO = {
		name: "TOPPING_MAYO",
		type: "topping",
		price: 20,
		calories: 5,
	};

	static TOPPING_SPICE = {
		name: "TOPPING_SPICE",
		type: "topping",
		price: 15,
		calories: 0,
	};

	addTopping(topping) {
		if (this.toppings.includes(topping)) {
			throw new HamburgerException("Duplicate topping detected.");
		} else if (topping.hasOwnProperty("type") && topping.type === "topping") {
			this.toppings.push(topping);
		} else {
			throw new HamburgerException(
				"Incorrect data for adding topping. Please check you data or create new Topping."
			);
		}
	}

	removeAllTopping() {
		this.toppings = [];
	};

	removeTopping(topping) {
		let index = this.toppings.indexOf(topping);

    if (this.toppings.includes(topping)) {
        this.toppings.splice(index, 1);
    } else {
        throw new HamburgerException("Topping not found");
    }
	}

	get getToppings() {

		return this.toppings;
	}

	get getSize() {
		return this.size;
	}

	get getStuffing() {
		return this.stuffing;
	}

	get calculatePrice() {
		let toppingsPrice = 0;

		if (this.toppings) {
			for (let i = 0; i < this.toppings.length; i++) {
				toppingsPrice += this.toppings[i]["price"];
			}
		}

		return this.size["price"] + this.stuffing["price"] + toppingsPrice;
	}

	get calculateCalories() {
		let toppingsCalories = 0;

		if (this.toppings) {
			for (let i = 0; i < this.toppings.length; i++) {
				toppingsCalories += this.toppings[i]["calories"];
			}
		}
		return (
			this.size["calories"] + this.stuffing["calories"] + toppingsCalories
		);
	}
}

class HamburgerException extends Error {
	constructor(message) {
		super();
		this.message = message;
	}
}

const royalCBR = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
// const royalCBR2 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.TOPPING_SPICE);
// console.log(royalCBR2);
// console.log(royalCBR);

// royalCBR.addTopping(Hamburger.TOPPING_SPICE);
royalCBR.addTopping(Hamburger.TOPPING_SPICE);
royalCBR.addTopping(Hamburger.TOPPING_MAYO);
// royalCBR.addTopping(Hamburger.TOPPING);
// console.log(royalCBR);

// console.log(`Price: ${royalCBR.calculatePrice}`);
// console.log(`Calories: ${royalCBR.calculateCalories}`)
// console.log(`Hamburger size: ${royalCBR.getSize.name}`);
// console.log(royalCBR.getSize);
// console.log(royalCBR.getToppings);

// royalCBR.getToppings.forEach(element => {
// 	console.log(element.name);
// });

// console.log(royalCBR.removeTopping(Hamburger.TOPPING_MAYO));
// royalCBR.removeAllTopping();
// royalCBR.removeTopping(Hamburger.TOPPING_MAYO);
// royalCBR.removeTopping(Hamburger.TOPPING_SPICE);

// console.log(royalCBR);
